//
//  ViewController.swift
//  HS
//
//  Created by ABEL on 2019/4/12.
//  Copyright © 2019 ABEL. All rights reserved.
//


import Cocoa

class ViewController: NSViewController {

    @IBOutlet weak var path: NSTextField!
    
    @IBOutlet var codeee: NSTextView!

    @IBAction func startReplace(_ sender: Any) {
        if  FileManager.default.fileExists(atPath: path.stringValue) {
            codeee.string = ""
            findLib(sourcePath: path.stringValue)
            codeee.string = codeee.string + "完成\n"
        }else{
            codeee.string = "地址无效，请重新输入"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}


extension ViewController{
    
    //找到lib文件夹
    func findLib(sourcePath:String?) {
        guard let path = sourcePath else {
            print("路径不能为空","\n")
            return
        }
        if  FileManager.default.fileExists(atPath: path) {
            do {
                let files = try FileManager.default.contentsOfDirectory(atPath: path)
                for dic in files{
                    if dic == ".DS_Store" {
                        
                    }else {
                        do {
                            let childPath = path + "/" + dic
                            let info = try FileManager.default.attributesOfItem(atPath: childPath)
                            let isDic :String = info[FileAttributeKey.type] as! String
                            if isDic == FileAttributeType.typeDirectory.rawValue && childPath.contains("lib"){
                                readFileFour(sourcePath: childPath)
                            }
                        }
                        catch{
                            print(error,"请结束")
                        }
                    }
                }
            }
            catch  {
                print("空文件",error,"请结束")
            }
        }else{
            print("文件不存在",path as Any,"请结束")
        }
    }
    
    //读取文件 和 文件夹 递归
    func readFileFour(sourcePath:String?) {
        guard let path = sourcePath else {
            print("路径不能为空","\n")
            return
        }
        if  FileManager.default.fileExists(atPath: path) {
            //            print("文件存在",path as Any)
            do {
                let files = try FileManager.default.contentsOfDirectory(atPath: path)
                for dic in files{
                    //                    print(dic)
                    if dic == ".DS_Store" {
                        //                        print("无效的类型",dic  ,"\n")
                    }else {
                        do {
                            let childPath = path + "/" + dic
                            let info = try FileManager.default.attributesOfItem(atPath: childPath)
                            let isDic :String = info[FileAttributeKey.type] as! String
                            if isDic == FileAttributeType.typeDirectory.rawValue {
                                readFileFour(sourcePath: childPath)
                            }else if childPath.hasSuffix(".g.dart"){
                                var code = Tool.readFileForStr(path:childPath)
                                if code.contains(" as String"){
                                    code = (code as NSString).replacingOccurrences(of: " as String", with: ".toString()")
                                    codeee.string = codeee.string + "修改了" + childPath + "\n"
                                    
                                    do{
                                        try FileManager.default.removeItem(at: URL.init(fileURLWithPath: childPath))
                                        let data = code.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                                        let isSucces = FileManager.default.createFile(atPath: childPath, contents:data, attributes: nil)
                                        if  !isSucces {
                                            print("文件写入失败，请停止")
                                        }else{
                                            print("文件写入成功")
                                        }
                                    }catch{
                                        print(error)
                                        print("删除老的文件失败")
                                    }
                                }
                            }
                        }
                        catch{
                            print(error,"请结束")
                        }
                    }
                }
                ////
            }
            catch  {
                print("空文件",error,"请结束")
            }
        }else{
            print("文件不存在",path as Any,"请结束")
        }
    }

}


class Tool {
   class func readFileForStr(path:String) -> String {
        let strData:NSData? = NSData.init(contentsOfFile: path)
        guard let _  = strData else {
            print("读取失败：",path ,"\n")
            return ""
        }
        let str:String? = String(data: strData! as Data, encoding: .utf8)
        guard let _  = str else {
            print(path,"没有代码"  ,"\n")
            return ""
        }
        return  str!
    }
}
