//
//  ehight.swift
//  HS
//
//  Created by ABEL on 2019/4/19.
//  Copyright © 2019 ABEL. All rights reserved.
//

import Foundation

var chageFuncNamefunAllArry = [String]()
let funcKey = ["numberOfItems","pagerView","viewDidLoad","init","mapping","tableView","tabBarController","photoBrowser","viewWillAppear","imagePickerController","pushViewController","numberOfSections","viewWillDisappear","renderer","didAdd","anchor","didRemove","didUpdate"] //
extension ViewController{
    //查找函数名
    func findchageFuncName(path:String?) {
//        print("替换函数参数")
        
        guard path != nil && !path!.isEmpty else {
            return print("路径不能为空"  ,"\n")
        }
        //函数名关键词
        if  FileManager.default.fileExists(atPath: path!) {
            do {
                let files = try FileManager.default.contentsOfDirectory(atPath: path!)
                for dic in files{
                    if dic == ".DS_Store" {
                        
                    }else {
                        let childPath = path! + "/" + dic
                        let info = try FileManager.default.attributesOfItem(atPath: childPath)
                        let isDic :String = info[FileAttributeKey.type] as! String
                        
                        if isDic == FileAttributeType.typeDirectory.rawValue {
                            findchageFuncName(path: childPath)
                        }else{
                            let code = Tool.readFileForStr(path:childPath)
                            let WriCode =  NSMutableString.init(string: code)
                            let resWill = code.textRegex(pattern:"(?-i).*func [A-Za-z]+.*\\(.*\\)")
                            for one in resWill{
//                                print(WriCode.substring(with: one.range))
                                let funcNameRange = code.textForFuncRegex(pattern:"(?-i)[A-Za-z]+ *\\(",range:one.range).first
                                let funcNameLast = code.textForFuncRegex(pattern:"(?-i)[A-Za-z]+",range:funcNameRange!.range)
                                let funcNameStr = WriCode.substring(with: funcNameLast.last!.range)
                                if !(funcKey.contains(funcNameStr)) && !(chageFuncNamefunAllArry.contains(funcNameStr)){
                                    chageFuncNamefunAllArry.append(funcNameStr)
                                    
//                                    let typeRange = code.textForFuncRegex(pattern:"(?-i): *[A-Za-z]+ *,",range:one.range)
//                                    let lastTypeRange = code.textForFuncRegex(pattern:"(?-i): *[A-Za-z]+ *\\)",range:one.range)
//                                    var allType = [NSTextCheckingResult]()
//                                    allType = allType + typeRange + lastTypeRange
                                    
                                    
                                    let oneVarRange = code.textForFuncRegex(pattern:"(?-i)\\( *[A-Za-z]+ *:",range:one.range)
                                    let otherVarRange = code.textForFuncRegex(pattern:"(?-i)\\, *[A-Za-z]+ *:",range:one.range)
                                    var allVar = [NSTextCheckingResult]()
                                    allVar = allVar + oneVarRange
                                    allVar = allVar + otherVarRange
                                    
                                    
                                    if !allVar.isEmpty{  //无参函数不用处理
                                        var fullfuncName = String(format: "(?-i)%@ *\\(", funcNameStr)
                                        for tmp in allVar {
                                            let typeRange = code.textForFuncRegex(pattern:"(?-i)[A-Za-z]+",range:tmp.range)
                                            let tyeeee = WriCode.substring(with: typeRange.last!.range)
                                            
                                            fullfuncName = fullfuncName + " *" + tyeeee + " *:" + " *[A-Za-z]+ *"
                                            
                                            if tmp != allVar.last {
                                                fullfuncName = fullfuncName + ","
                                            }
                                        }
                                        fullfuncName = fullfuncName + "\\)"
//                                        print(fullfuncName)
                                        

                                        
                                        var allVarStr = [String]()
                                        var allVarWillStr = [String]()
                                        for varRange in allVar{
                                            let typeRangejj = code.textForFuncRegex(pattern:"(?-i)[A-Za-z]+",range:varRange.range)
                                            let tyeeeejj = WriCode.substring(with: typeRangejj.last!.range)
                                            allVarStr.append(tyeeeejj)
                                            allVarWillStr.append(String.randomStr(len: rangLeng))
                                        }
                                        chageFuncName(path:path,funcName:fullfuncName,place:allVarStr,willStr:allVarWillStr)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch  {
                print("空文件",error,"请结束\n\n\n\n\n\n\n\n\n\n\n\n\n")
            }
        }else{
            print("文件不存在",path as Any,"请结束\n\n\n\n\n\n\n\n\n\n\n\n\n")
        }
    }
    
    
    //替换函数名
    func chageFuncName(path:String?,funcName:String,place:[String],willStr:[String]) {
//        print("替换函数参数 开始")
        
        guard path != nil && !path!.isEmpty else {
            return print("路径不能为空"  ,"\n")
        }
        //函数名关键词
        
        
        if  FileManager.default.fileExists(atPath: path!) {
            //            print("文件存在",path as Any)
            do {
                let files = try FileManager.default.contentsOfDirectory(atPath: path!)
                for dic in files{
                    //                    print(dic)
                    if dic == ".DS_Store" {
                        //                        print("无效的类型",dic  ,"\n")
                    }else {
                        let code = Tool.readFileForStr(path:path!)
                        var WriCode =  NSMutableString.init(string: code)
                        
                        var isNeedWri = false
                        for wiPlace in place {
                            let resWill = code.textRegex(pattern: "(?-i)" + wiPlace)
                            if !resWill.isEmpty {
                                for xii in 0..<resWill.count{
                                    let one = resWill[resWill.count - xii - 1]
                                    let stry = willStr[xii]
                                    if one.range.location > 0 && one.range.location < WriCode.length {
                                        let pre = WriCode.substring(with: NSRange.init(location: one.range.location - 1, length: 1))
                                        let nex = WriCode.substring(with: NSRange.init(location: one.range.location  + one.range.length, length: 1))
                                        if !pre.isPureCharacters() && !nex.isPureCharacters(){
                                            let tofileStr = WriCode.replacingCharacters(in: one.range, with: stry)
                                            WriCode =  NSMutableString.init(string: tofileStr)
                                            isNeedWri = true
                                        }
                                    }
                                }
                                
                            }
                        }
                        if isNeedWri {
                            let uurl = WriCode.substring(from: 0)
                            let datatoFile: NSData = uurl.data(using: .utf8)! as NSData
                            
                            let isSucces = FileManager.default.createFile(atPath: path!, contents: datatoFile as Data, attributes: nil)  //[FileAttributeKey.type :
                            if  isSucces {
                                print("写入成功")
                            }else{
                                print("\n\n\nn\n\n\n\n文件改名失败，请停止\n\n\n\n\n\n\n\n\n\n\n")
                            }
                        }
                    /*
                        let childPath = path! + "/" + dic
                        let info = try FileManager.default.attributesOfItem(atPath: childPath)
                        let isDic :String = info[FileAttributeKey.type] as! String
                        
                        if isDic == FileAttributeType.typeDirectory.rawValue {
                            chageFuncName(path:childPath,funcName:funcName,place:place,willStr:willStr)
                        }else{
                            let code = Tool.readFileForStr(path:childPath)
                            let WriCode =  NSMutableString.init(string: code)
                            let resWill = code.textRegex(pattern:funcName)
                            
                            var isneed = false
                            for xii in 0..<resWill.count{
                                let one = resWill[resWill.count - xii - 1]
                                print(WriCode.substring(with: one.range))
                                for num in 0..<place.count{
                                    let varr = place[num]
                                    let resWilllll = code.textForFuncRegex(pattern:"(?-i)" + varr,range:one.range)
                                    for xiilll in 0..<resWilllll.count{
                                        let oneiiii = resWilllll[resWilllll.count - xiilll - 1]
                                        WriCode.replaceCharacters(in: oneiiii.range, with: willStr[num])
                                        isneed = true
                                    }
                                }
                            }
                            if isneed {
                                let data = WriCode.data(using: String.Encoding.utf8.rawValue)
                                let isSucces = FileManager.default.createFile(atPath: childPath, contents:data, attributes: nil)
                                if  isSucces {
                                    print("写入成功")
                                }else{
                                    print("\n\n\nn\n\n\n\n文件写入失败，请停止\n\n\n\n\n\n\n\n\n\n\n")
                                }
                            }
                        }
                         */
                    }
                }
            }
            catch  {
                print("空文件",error,"请结束\n\n\n\n\n\n\n\n\n\n\n\n\n")
            }
        }else{
            print("文件不存在",path as Any,"请结束\n\n\n\n\n\n\n\n\n\n\n\n\n")
        }
    }
}
