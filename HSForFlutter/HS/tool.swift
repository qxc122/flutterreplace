//
//  tool.swift
//  HS
//
//  Created by ABEL on 2019/4/13.
//  Copyright © 2019 ABEL. All rights reserved.
//

import Foundation


var alllllll = [String]()
class Tool {
   class func readFileForStr(path:String) -> String {
        let strData:NSData? = NSData.init(contentsOfFile: path)
        guard let _  = strData else {
            print("读取失败：",path ,"\n")
            return ""
        }
        let str:String? = String(data: strData! as Data, encoding: .utf8)
        guard let _  = str else {
            print(path,"没有代码"  ,"\n")
            return ""
        }
        return  str!
    }
}
extension Tool{
    //修改文件名
     class func editFileName(path:String,name:String) -> String {
        do {
            //            let pathAll = path + "/" + name
            //
            //            let strData:NSData? = NSData.init(contentsOfFile: pathAll)
            //            let new = path + "/" + String.randomStr(len: 10) + ".m"
            //            let isSucces = FileManager.default.createFile(atPath: new, contents: strData as Data?, attributes: nil)  //[FileAttributeKey.type : FileAttributeType.typeDirectory]
            //
            //            if  !isSucces {
            //                print("\n\n\nn\n\n\n\n文件改名失败，请停止\n\n\n\n\n\n\n\n\n\n\n")
            //            }
            //            try FileManager.default.removeItem(atPath: pathAll)
            
            let pathAll = path + "/" + name
            let randoDic = String.randomStr(len: rangLeng) + ".swift"
            let new = path + "/" + randoDic
            
            let strData:NSData? = NSData.init(contentsOfFile: pathAll)
            let isSucces = FileManager.default.createFile(atPath: new, contents: strData as Data?, attributes: nil)
            if  !isSucces {
                print("\n\n\nn\n\n\n\n文件改名失败，请停止\n\n\n\n\n\n\n\n\n\n\n")
                return ""
            }
            
            try FileManager.default.replaceItem(at: URL.init(fileURLWithPath: new), withItemAt: URL.init(fileURLWithPath: pathAll), backupItemName: randoDic, options: FileManager.ItemReplacementOptions.usingNewMetadataOnly, resultingItemURL: nil)
            return randoDic
        }catch{
            print(error)
            print("\n\n\nn\n\n\n\n文件改名失败，请停止\n\n\n\n\n\n\n\n\n\n\n")
            return ""
        }
    }
    
    //修改目录名
    class func editDicName(path:String,name:String) -> String {
        do {
            let pathAll = path + "/" + name
            let randoDic = String.randomStr(len: rangLeng)
            let new = path + "/" + randoDic
            try FileManager.default.createDirectory(atPath: new, withIntermediateDirectories: true, attributes: nil)
            
            try FileManager.default.replaceItem(at: URL.init(fileURLWithPath: new), withItemAt: URL.init(fileURLWithPath: pathAll), backupItemName: randoDic, options: FileManager.ItemReplacementOptions.usingNewMetadataOnly, resultingItemURL: nil)
            return randoDic
        }catch{
            print(error ,"请结束\n\n\n\n\n\n\n\n\n\n\n\n\n")
            return ""
        }
    }
    
    
    //复制目录
    class func copyFileName(path:String,name:String) -> String {
        do {
            //            let pathAll = path + "/" + name
            //
            //            let strData:NSData? = NSData.init(contentsOfFile: pathAll)
            //            let new = path + "/" + String.randomStr(len: 10) + ".m"
            //            let isSucces = FileManager.default.createFile(atPath: new, contents: strData as Data?, attributes: nil)  //[FileAttributeKey.type : FileAttributeType.typeDirectory]
            //
            //            if  !isSucces {
            //                print("\n\n\nn\n\n\n\n文件改名失败，请停止\n\n\n\n\n\n\n\n\n\n\n")
            //            }
            //            try FileManager.default.removeItem(atPath: pathAll)
            
            let pathAll = path + "/" + name
            let randoDic = String.randomStr(len: rangLeng) + ".swift"
            let new = path + "/" + randoDic
            
//            let strData:NSData? = NSData.init(contentsOfFile: pathAll)
//            let isSucces = FileManager.default.createFile(atPath: new, contents: strData as Data?, attributes: nil)
//            if  !isSucces {
//                print("\n\n\nn\n\n\n\n文件改名失败，请停止\n\n\n\n\n\n\n\n\n\n\n")
//                return ""
//            }
//            try FileManager.default.copyItem(at: URL.init(fileURLWithPath: pathAll), to: URL.init(fileURLWithPath: new))
            try FileManager.default.copyItem(atPath: pathAll, toPath: new)
//            try FileManager.default.replaceItem(at: URL.init(fileURLWithPath: new), withItemAt: URL.init(fileURLWithPath: pathAll), backupItemName: randoDic, options: FileManager.ItemReplacementOptions.usingNewMetadataOnly, resultingItemURL: nil)
            return randoDic
        }catch{
            print(error)
            print("\n\n\nn\n\n\n\n文件改名失败，请停止\n\n\n\n\n\n\n\n\n\n\n")
            return ""
        }
    }
}

extension String {
    func transformToPinYin() -> String {
        let mutableString = NSMutableString(string: self)
        //把汉字转为拼音
        CFStringTransform(mutableString, nil, kCFStringTransformToLatin, false)
        //去掉拼音的音标
        CFStringTransform(mutableString, nil, kCFStringTransformStripDiacritics, false)
        let string = String(mutableString)
        //去掉空格
        return string.replacingOccurrences(of: " ", with: "")
    }
}


extension String{
    static let random_str_characters = allHanzi
    static func randomStr(len : Int) -> String{
        
        var allLen = 20
        let Min = 2
        if len > Min{
            let tmp = Int(arc4random_uniform(UInt32(random_str_characters.count)))
            allLen = Min + tmp%(len - Min)
        }
        
        var ranStr = ""
        for _ in 0..<allLen {
            let index = Int(arc4random_uniform(UInt32(random_str_characters.count)))
            ranStr.append(random_str_characters[random_str_characters.index(random_str_characters.startIndex, offsetBy: index)])
        }
        let resss = ranStr.transformToPinYin()
        if alllllll.contains(resss) {
            return randomStr(len:len)
        }else{
            alllllll.append(resss)
            return resss
        }
    }
    
    
    // 1.匹配纯文本
    func textRegex(pattern: String) -> [NSTextCheckingResult]{
        
        do {
            // 1.1.定义规则
            //let pattern = "ben"
            // 1.2.创建正则表达式对象
            let regex = try NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options.caseInsensitive)
            // 1.3.开始匹配
            let res = regex.matches(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count))
            
            return res
            
        } catch {
            print(error ,"请结束\n\n\n\n\n\n\n\n\n\n\n\n\n")
            return []
        }
    }
    
    // 1.匹配纯文本
    func textForFuncRegex(pattern: String,range:NSRange) -> [NSTextCheckingResult]{
        
        do {
            // 1.1.定义规则
            //let pattern = "ben"
            // 1.2.创建正则表达式对象
            let regex = try NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options.caseInsensitive)
            // 1.3.开始匹配
            let res = regex.matches(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: range)
            
            return res
            
        } catch {
            print(error ,"请结束\n\n\n\n\n\n\n\n\n\n\n\n\n")
            return []
        }
    }
}



extension NSMutableString{
    // 1.匹配纯文本
    func textRegex(pattern: String) -> [NSTextCheckingResult]{
        
        do {
            // 1.1.定义规则
            //let pattern = "ben"
            // 1.2.创建正则表达式对象
            let regex = try NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options.caseInsensitive)
            // 1.3.开始匹配
            let res = regex.matches(in: self as String, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.length))
            
            return res
            
        } catch {
            print(error ,"请结束\n\n\n\n\n\n\n\n\n\n\n\n\n")
            return []
        }
    }
    
    // 1.匹配纯文本
    func textForFuncRegex(pattern: String,range:NSRange) -> [NSTextCheckingResult]{
        
        do {
            // 1.1.定义规则
            //let pattern = "ben"
            // 1.2.创建正则表达式对象
            let regex = try NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options.caseInsensitive)
            // 1.3.开始匹配
            let res = regex.matches(in: self as String, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: range)
            
            return res
            
        } catch {
            print(error ,"请结束\n\n\n\n\n\n\n\n\n\n\n\n\n")
            return []
        }
    }
}

extension String{
    func isPureCharacters() -> Bool {
        let resWill = self.textRegex(pattern: "[A-Za-z]+")
        if resWill.count == 1 {
            return true
        }
        return false
    }
}




////第三步
//extension ViewController{
//    //读取文件 和 文件夹 递归
//    func readFileThreeNow(path:String?) {
//        guard path != nil && !path!.isEmpty else {
//            return print("路径不能为空"  ,"\n")
//        }
//        if  FileManager.default.fileExists(atPath: path!) {
//            //            print("文件存在",path as Any)
//            do {
//                let files = try FileManager.default.contentsOfDirectory(atPath: path!)
//                for dic in files{
//                    //                    print(dic)
//                    if dic == ".DS_Store" {
//                        //                        print("无效的类型",dic  ,"\n")
//                    }else {
//                        do {
//                            let childPath = path! + "/" + dic
//                            let info = try FileManager.default.attributesOfItem(atPath: childPath)
//                            let isDic :String = info[FileAttributeKey.type] as! String
//
//                            if isDic == FileAttributeType.typeDirectory.rawValue {
//                                readFileThreeNow(path: childPath)
//                            }else{
//
//                                //找类名
//                                let code = Tool.readFileForStr(path:childPath)
//                                var WriCode =  NSMutableString.init(string: code)
//
//                                var indexA = 0
//                                for key in arryClass{
//                                    //                                    WriCode.replaceOccurrences(of: key , with: arryClass2[indexA], options: NSString.CompareOptions.forcedOrdering, range: NSRange.init(location: 0, length: WriCode.length))
//                                    WriCode = NSMutableString.init(string: WriCode.replacingOccurrences(of: key, with: arryClass2[indexA]))
//                                    indexA += 1
//                                }
//
//                                var indexB = 0
//                                for key in arryFunc{
//                                    //                                    WriCode.replaceOccurrences(of: key, with: arryFunc2[indexB], options: NSString.CompareOptions.forcedOrdering, range: NSRange.init(location: 0, length: WriCode.length))
//                                    WriCode = NSMutableString.init(string: WriCode.replacingOccurrences(of: key, with: arryFunc2[indexB]))
//                                    indexB += 1
//
//                                }
//
//                                var indexC = 0
//                                for key in arryVar{
//                                    //                                    WriCode.replaceOccurrences(of: key, with: arryFunc2[indexB], options: NSString.CompareOptions.forcedOrdering, range: NSRange.init(location: 0, length: WriCode.length))
////                                    WriCode = NSMutableString.init(string: WriCode.replacingOccurrences(of: key, with: arryVar2[indexC]))
//                                    indexC += 1
//
//                                }
//
//                                let uurl = WriCode.substring(from: 0)
//                                let datatoFile: NSData = uurl.data(using: .utf8)! as NSData
//
//                                let isSucces = FileManager.default.createFile(atPath: childPath, contents: datatoFile as Data, attributes: nil)  //[FileAttributeKey.type :
//                                if  isSucces {
//                                    print("写入成功")
//                                }else{
//                                    print("\n\n\nn\n\n\n\n文件改名失败，请停止\n\n\n\n\n\n\n\n\n\n\n")
//                                }
//                            }
//                        }
//                        catch{
//                            print(error,"请结束\n\n\n\n\n\n\n\n\n\n\n\n\n")
//                        }
//                    }
//                }
//            }
//            catch  {
//                print("空文件",error,"请结束\n\n\n\n\n\n\n\n\n\n\n\n\n")
//            }
//        }else{
//            print("文件不存在",path as Any,"请结束\n\n\n\n\n\n\n\n\n\n\n\n\n")
//        }
//    }
//}
