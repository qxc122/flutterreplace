//
//  ViewController.swift
//  HS
//
//  Created by ABEL on 2019/4/12.
//  Copyright © 2019 ABEL. All rights reserved.
//


import Cocoa

class ViewController: NSViewController {

    @IBOutlet weak var path: NSTextField!
    
    @IBOutlet var codeee: NSTextView!

    @IBAction func startReplace(_ sender: Any) {
        guard !path.stringValue.isEmpty else {
            codeee.string = "请输入json字符串"
            return
        }
        guard let dic = convertStringToDictionary(text: path.stringValue) else {
            codeee.string = "不是一个有效的json 字符串 "
            return
        }

        var res = "struct Model: Codable {\n"
        for (key,zhi) in dic {
            if let _ = zhi as? [String: Any]{
                res = res + "var \(key): \(key.capitalized)Model?\n"
            }else if let _ = zhi as? [[String: Any]]{
                res = res + "var \(key): [\(key.capitalized)Model]?\n"
            }else if let _ = zhi as? [String]{
                res = res + "var \(key): [String]?\n"
            }else{
                if zhi is NSNumber{
                    res = res + "var \(key): NSNumber?\n"
                }else {
                    res = res + "var \(key): String?\n"
                }
            }
        }



        res = res + "}\n"

        
        codeee.string = res

        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
    func convertStringToDictionary(text: String) -> [String:Any]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: [JSONSerialization.ReadingOptions.init(rawValue: 0)]) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
}
