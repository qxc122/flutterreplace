//
//  five.swift
//  HS
//
//  Created by ABEL on 2019/4/16.
//  Copyright © 2019 ABEL. All rights reserved.
//

import Foundation

extension ViewController {
    
    //生成变量
    func creatVar(path:String?) {
        guard path != nil && !path!.isEmpty else {
            return print("路径不能为空"  ,"\n")
        }
        if  FileManager.default.fileExists(atPath: path!) {
            //            print("文件存在",path as Any)
            do {
                let files = try FileManager.default.contentsOfDirectory(atPath: path!)
                for dic in files{
                    //                    print(dic)
                    if dic == ".DS_Store" {
                        //                        print("无效的类型",dic  ,"\n")
                    }else {
                        let childPath = path! + "/" + dic
                        let info = try FileManager.default.attributesOfItem(atPath: childPath)
                        let isDic :String = info[FileAttributeKey.type] as! String
                        
                        if isDic == FileAttributeType.typeDirectory.rawValue {
                            if isClass && isFunc(){
                                creatFile(childPath: childPath)
                            }
                            creatVar(path: childPath)
                        }else{
                            let code = Tool.readFileForStr(path:childPath)
                            let WriCode =  NSMutableString.init(string: code)
                            let resWill = code.textRegex(pattern:"(?-i)func .*\\{")
                            for xii in 0..<resWill.count{
                                let one = resWill[resWill.count - xii - 1]
                                
                                let num = getFuncAndVarNum()
                                for _ in 0..<num{
                                    let varStr = creatVarStr()
                                    WriCode.insert(varStr, at: one.range.location + one.range.length)
                                }
                            }
                            
                            
                            let resWillClass = WriCode.substring(to: WriCode.length).textRegex(pattern:"(?-i)class .*\\{")
                            for xiiii in 0..<resWillClass.count{
                                let oneee = resWillClass[resWillClass.count - xiiii - 1]

                                let nummm = getFuncAndVarNum()
                                for _ in 0..<nummm{
                                    if isFunc(){
                                        let varStr = creatVarStr()
                                        WriCode.insert(varStr, at: oneee.range.location + oneee.range.length)
                                    }else{
                                        let funcStr = creatFuncStr()
                                        WriCode.insert(funcStr, at: oneee.range.location + oneee.range.length)
                                    }
                                }
                            }
                            
                            let data = WriCode.data(using: String.Encoding.utf8.rawValue)
                            
                            let host : NSString = childPath as NSString
                            
                            
                            let randoDic = String.randomStr(len: rangLeng) + ".swift"
                            let new = host.deletingLastPathComponent + "/" + randoDic
                            
                            
                            let isSucces = FileManager.default.createFile(atPath: new, contents:data, attributes: nil)
                            if  !isSucces {
                                print("\n\n\nn\n\n\n\n文件写入失败，请停止\n\n\n\n\n\n\n\n\n\n\n")
                            }else{
                                do{
                                    try FileManager.default.removeItem(at: URL.init(fileURLWithPath: childPath))
                                }catch{
                                    print(error)
                                    print("\n\n\nn\n\n\n\n文件写入失败，请停止\n\n\n\n\n\n\n\n\n\n\n")
                                }
                            }
                            if isClass && isFunc(){
                                creatFile(childPath: host.deletingLastPathComponent)
                            }
                        }
                    }
                }
            }
            catch  {
                print("空文件",error,"请结束\n\n\n\n\n\n\n\n\n\n\n\n\n")
            }
        }else{
            print("文件不存在",path as Any,"请结束\n\n\n\n\n\n\n\n\n\n\n\n\n")
        }
    }
    
    //添加新的文件
    func creatFile(childPath:String) {
        let WriCode =  NSMutableString.init(string: "\n\n\n\n\n\n\n\n\n\n")
        let classNNname = String.randomStr(len: rangLeng)
        WriCode.append(String(format: "\nclass %@ {\n", classNNname ))
        
        
        let num = getClassFuncAndVarNum()
        for _ in 0..<num{
            if isFunc(){
                let funcStr = creatFuncStr()
                WriCode.append(funcStr)
            }else{
                let varStr = creatVarStr()
                WriCode.append(varStr)
            }
        }
        WriCode.append(String(format: "\n}\n"))
        let data = WriCode.data(using: String.Encoding.utf8.rawValue)
        let randoDic = String.randomStr(len: rangLeng) + ".swift"
        let new = childPath + "/" + randoDic
        let isSucces = FileManager.default.createFile(atPath: new, contents:data, attributes: nil)
        if  !isSucces {
            print("\n\n\nn\n\n\n\n文件写入失败，请停止\n\n\n\n\n\n\n\n\n\n\n")
        }
    }
}
