//
//  GuojihuaVc.swift
//  HS
//
//  Created by Power IT3 on 2022/7/23.
//  Copyright © 2022 ABEL. All rights reserved.
//

import Cocoa

class GuojihuaVc: NSViewController {

    @IBOutlet weak var kaishi: NSButton!
    @IBOutlet weak var qingkong: NSButton!

    @IBOutlet weak var zhongwen: NSScrollView!
    
    @IBOutlet weak var res: NSScrollView!
    @IBOutlet weak var yingwen: NSScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        let z = res.documentView as? NSTextView
        z?.string = "语言国际化用到的\n左边输入中文\n 右边英文"
        
    }
    @IBAction func clickss(_ sender: NSButton) {
        if  sender === qingkong{
            let z = zhongwen.documentView as? NSTextView
            let y = yingwen.documentView as? NSTextView
            z?.string = ""
            y?.string = ""
            
            let r = res.documentView as? NSTextView
            r?.string = "语言国际化用到的\n左边输入中文\n 右边英文"
        }else if  sender === kaishi{
            let z = zhongwen.documentView as? NSTextView
            let y = yingwen.documentView as? NSTextView
            guard let zs = z?.string as? NSString else{
                let z = res.documentView as? NSTextView
                z?.string = "请输入中文"
                return
            }
            guard let ys = y?.string  as? NSString else{
                let z = res.documentView as? NSTextView
                z?.string = "请输入英文"
                return
            }
            let zs1 = zs.replacingOccurrences(of: "  ", with: "")
            let zarry = zs1.split(separator: "\n")
            
            let ys1 = ys.replacingOccurrences(of: "  ", with: "")
            let yarry = ys1.split(separator: "\n")
            
            
            guard zarry.count == yarry.count else{
                let z = res.documentView as? NSTextView
                z?.string = "发生错误  中文 英文 条数不一样"
                return
            }
            
//            "99%的用户已更新" = "99%的用户已更新";
            var alls = ""
            for z in zarry{
                alls += "\"\(z)\" = \"\(z)\";\n"
            }
            
            alls += "\n\n\n\n"
            var index = 0
            for z in zarry{
                alls += "\"\(z)\" = \"\(yarry[index])\";\n"
                index += 1
            }
            
            let r = res.documentView as? NSTextView
            r?.string = alls
            
            ssss.copyStringToPasteboard(string: alls)
        }
        
    }
    let ssss =  zhuanhuanTool()

    
}
