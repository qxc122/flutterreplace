//
//  ChangeImagesNameVc.swift
//  HS
//
//  Created by Power IT3 on 2022/7/23.
//  Copyright © 2022 ABEL. All rights reserved.
//

import Cocoa

class ChangeImagesNameVc: NSViewController {
    @IBOutlet weak var path: NSTextField!
    
    @IBOutlet weak var info: NSTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "图片改名"
        path.stringValue = "/Users/power/Documents/GitHub/kotvgj/box"
        // Do view setup here.
    }
    let manager = FileManager.default

    @IBAction func btnsClickTap(_ sender: NSButton) {
        info.stringValue = ""
        let pas = path.stringValue
        guard !pas.isEmpty else{
            info.stringValue = "请输入项目地址"
            return
        }
        let contentsOfPath =  manager.enumerator(atPath: pas)
        for item in contentsOfPath?.allObjects ?? [] {
            guard let pngpath = item as? String else {
                continue
            }
            guard !pngpath.contains(".bundle") else {
                //说明是第三方库的 不动它
                continue
            }
            guard pngpath.hasSuffix(".png") else {
                continue
            }
//            let tttt = pngpath.replacingOccurrences(of: ".png", with: "")
//            guard manager.fileExists(atPath: tttt) else {
//                continue
//            }
            if !FileManager.directoryIsExists(path: pngpath){
                
                movepng(pngpath: pngpath)
                
            }
        }
    }
    
    func movepng(pngpath:String){
        let arry = pngpath.split(separator: "/")
        let path = pngpath.replacingOccurrences(of: arry.last ?? "", with: "")
        
//        try manager.moveItem(at: path, to: path) // 拷贝文件内容

        
        manager.changeCurrentDirectoryPath(path)
//        print(path)
        print(manager.currentDirectoryPath)
    }
    
    @IBOutlet weak var luajnmaBtn: NSButton!
    @IBOutlet weak var gaimingBtn: NSButton!
    
}

extension String{
    static let random_str_characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    static func randomStr(len : Int) -> String{
        var ranStr = ""
        for _ in 0..<len {
            let index = Int(arc4random_uniform(UInt32(random_str_characters.count)))
            ranStr.append(random_str_characters[random_str_characters.index(random_str_characters.startIndex, offsetBy: index)])
        }
        return ranStr
    }
}
extension FileManager {



   // 判断是否是文件夹的方法

   static func directoryIsExists (path: String) -> Bool {

       var directoryExists = ObjCBool.init(false)

       let fileExists = FileManager.default.fileExists(atPath: path, isDirectory: &directoryExists)



       return fileExists && directoryExists.boolValue

   }

}
