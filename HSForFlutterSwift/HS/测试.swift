//
//## 获取签名
//
//
//**接口地址**:`/consumer/common/im/getWebSignature`
//
//
//**请求方式**:`POST`
//
//
//**请求数据类型**:`application/json`
//
//
//**响应数据类型**:`*/*`
//
//
//**接口描述**:
//
//
//**请求示例**:
//
//
//```javascript
//{
//    "randomStr": "",
//    "timestamp": ""
//}
//```
//
//
//**请求参数**:
//
//
//| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
//| -------- | -------- | ----- | -------- | -------- | ------ |
//|signatureDTO|IM获取签名信息对象|body|true|SignatureDTO|SignatureDTO|
//|&emsp;&emsp;randomStr|随机数||false|string||
//|&emsp;&emsp;timestamp|时间戳||false|string||
//
//
//**响应状态**:
//
//
//| 状态码 | 说明 | schema |
//| -------- | -------- | ----- |
//|200|OK||
//|201|Created||
//|401|Unauthorized||
//|403|Forbidden||
//|404|Not Found||
//
//
//**响应参数**:
//
//
//暂无
//
//
//**响应示例**:
//```javascript
//
//```
//
//
//# 上传接口
//
//
//## 获取七牛云客户端上传凭证
//
//
//**接口地址**:`/consumer/common/upload/getUploadToken`
//
//
//**请求方式**:`GET`
//
//
//**请求数据类型**:`*`
//
//
//**响应数据类型**:`*/*`
//
//
//**接口描述**:
//
//
//**请求参数**:
//
//
//暂无
//
//
//**响应状态**:
//
//
//| 状态码 | 说明 | schema |
//| -------- | -------- | ----- |
//|200|OK|接口返回对象«七牛云客户端上传凭证»|
//|401|Unauthorized||
//|403|Forbidden||
//|404|Not Found||
//
//
//**响应参数**:
//
//
//| 参数名称 | 参数说明 | 类型 | schema |
//| -------- | -------- | ----- |----- |
//|code|返回代码|integer(int32)|integer(int32)|
//|message|返回处理消息|string||
//|onlTable||string||
//|result|返回数据对象|七牛云客户端上传凭证|七牛云客户端上传凭证|
//|&emsp;&emsp;host|文件url域名|string||
//|&emsp;&emsp;token|上传凭证|string||
//|success|成功标志|boolean||
//|timestamp|时间戳|integer(int64)|integer(int64)|
//
//
//**响应示例**:
//```javascript
//{
//    "code": 0,
//    "message": "",
//    "onlTable": "",
//    "result": {
//        "host": "",
//        "token": ""
//    },
//    "success": true,
//    "timestamp": 0
//}
//```
//
//
//
//# 极光IM服务
//
//
//## 联系商家/联系员工
//
//
//**接口地址**:`/consumer/common/im/getChatByBusinessGroupId`
//
//
//**请求方式**:`GET`
//
//
//**请求数据类型**:`*`
//
//
//**响应数据类型**:`*/*`
//
//
//**接口描述**:返回IM群ID
//
//
//**请求参数**:
//
//
//| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
//| -------- | -------- | ----- | -------- | -------- | ------ |
//|businessId|商户ID|query|false|string||
//|teamUserId|员工ID|query|false|string||
//|type|类型（0:商户，1:员工）|query|false|integer(int32)||
//
//
//**响应状态**:
//
//
//| 状态码 | 说明 | schema |
//| -------- | -------- | ----- |
//|200|OK|接口返回对象«IM群组»|
//|401|Unauthorized||
//|403|Forbidden||
//|404|Not Found||
//
//
//**响应参数**:
//
//
//| 参数名称 | 参数说明 | 类型 | schema |
//| -------- | -------- | ----- |----- |
//|code|返回代码|integer(int32)|integer(int32)|
//|message|返回处理消息|string||
//|onlTable||string||
//|result|返回数据对象|IM群组|IM群组|
//|&emsp;&emsp;groupId|群聊群组ID|string||
//|&emsp;&emsp;member|群聊成员信息|成员信息|成员信息|
//|&emsp;&emsp;&emsp;&emsp;avatar|头像|string||
//|&emsp;&emsp;&emsp;&emsp;id|成员ID|string||
//|&emsp;&emsp;&emsp;&emsp;nickname|昵称|string||
//|success|成功标志|boolean||
//|timestamp|时间戳|integer(int64)|integer(int64)|
//
//
//**响应示例**:
//```javascript
//{
//    "code": 0,
//    "message": "",
//    "onlTable": "",
//    "result": {
//        "groupId": "",
//        "member": {
//            "additionalProperties1": {
//                "avatar": "",
//                "id": "",
//                "nickname": ""
//            }
//        }
//    },
//    "success": true,
//    "timestamp": 0
//}
//```
//
//
//
//
//
//# 社区评论
//
//
//## 添加
//
//
//**接口地址**:`/consumer/community/comment/add`
//
//
//**请求方式**:`POST`
//
//
//**请求数据类型**:`application/json`
//
//
//**响应数据类型**:`*/*`
//
//
//**接口描述**:添加
//
//
//**请求示例**:
//
//
//```javascript
//{
//    "attachment": [
//        {
//            "cover": "",
//            "type": 0,
//            "url": ""
//        }
//    ],
//    "attachmentText": "",
//    "avatar": "",
//    "businessId": "",
//    "comment": "",
//    "contentId": "",
//    "createTime": "",
//    "id": "",
//    "isLikes": true,
//    "likes": 0,
//    "nickname": "",
//    "replyCommentId": "",
//    "replyNickname": "",
//    "replyText": "",
//    "replyUserId": "",
//    "review": "",
//    "status": 0,
//    "updateBy": "",
//    "updateTime": "",
//    "userId": ""
//}
//```
//
//**请求参数**:
//
//
//| 参数名称 | 参数说明 | in    | 是否必须 | 数据类型 | schema |
//| -------- | -------- | ----- | -------- | -------- | ------ |
//|communityComment|社区评论|body|true|社区评论对象|社区评论对象|
//|&emsp;&emsp;attachment|附件url（图片，视频）集合||false|array|附件类型|
//|&emsp;&emsp;&emsp;&emsp;cover|封面URL，视频时第一帧||false|string||
//|&emsp;&emsp;&emsp;&emsp;type|类型（0:图片,1:视频）||false|integer(int32)||
//|&emsp;&emsp;&emsp;&emsp;url|媒体URL||false|string||
//|&emsp;&emsp;attachmentText|评论附件||false|string||
//|&emsp;&emsp;avatar|头像||false|string||
//|&emsp;&emsp;businessId|商户ID||false|string||
//|&emsp;&emsp;comment|评论内容||false|string||
//|&emsp;&emsp;contentId|内容ID||false|string||
//|&emsp;&emsp;createTime|创建日期||false|string(date-time)||
//|&emsp;&emsp;id|主键||false|string||
//|&emsp;&emsp;isLikes|是否点赞||false|boolean||
//|&emsp;&emsp;likes|点赞数||false|integer(int32)||
//|&emsp;&emsp;nickname|昵称||false|string||
//|&emsp;&emsp;replyCommentId|回复原评论ID||false|string||
//|&emsp;&emsp;replyNickname|回复原来用户昵称||false|string||
//|&emsp;&emsp;replyText|回复原评论内容||false|string||
//|&emsp;&emsp;replyUserId|回复原评论用户ID||false|string||
//|&emsp;&emsp;review|审核意见||false|string||
//|&emsp;&emsp;status|评论审核状态(-1:审核失败,0:审核中,1:审核通过)||false|integer(int32)||
//|&emsp;&emsp;updateBy|更新人||false|string||
//|&emsp;&emsp;updateTime|更新日期||false|string(date-time)||
//|&emsp;&emsp;userId|用户主键ID||false|string||
//
//
//**响应状态**:
//
//
//| 状态码 | 说明 | schema |
//| -------- | -------- | ----- |
//|200|OK|接口返回对象«社区评论对象»|
//|201|Created||
//|401|Unauthorized||
//|403|Forbidden||
//|404|Not Found||
//
//
//**响应参数**:
//
//
//| 参数名称 | 参数说明 | 类型 | schema |
//| -------- | -------- | ----- |----- |
//|code|返回代码|integer(int32)|integer(int32)|
//|message|返回处理消息|string||
//|onlTable||string||
//|result|返回数据对象|社区评论对象|社区评论对象|
//|&emsp;&emsp;attachment|附件url（图片，视频）集合|array|附件类型|
//|&emsp;&emsp;&emsp;&emsp;cover|封面URL，视频时第一帧|string||
//|&emsp;&emsp;&emsp;&emsp;type|类型（0:图片,1:视频）|integer(int32)||
//|&emsp;&emsp;&emsp;&emsp;url|媒体URL|string||
//|&emsp;&emsp;attachmentText|评论附件|string||
//|&emsp;&emsp;avatar|头像|string||
//|&emsp;&emsp;businessId|商户ID|string||
//|&emsp;&emsp;comment|评论内容|string||
//|&emsp;&emsp;contentId|内容ID|string||
//|&emsp;&emsp;createTime|创建日期|string(date-time)||
//|&emsp;&emsp;id|主键|string||
//|&emsp;&emsp;isLikes|是否点赞|boolean||
//|&emsp;&emsp;likes|点赞数|integer(int32)||
//|&emsp;&emsp;nickname|昵称|string||
//|&emsp;&emsp;replyCommentId|回复原评论ID|string||
//|&emsp;&emsp;replyNickname|回复原来用户昵称|string||
//|&emsp;&emsp;replyText|回复原评论内容|string||
//|&emsp;&emsp;replyUserId|回复原评论用户ID|string||
//|&emsp;&emsp;review|审核意见|string||
//|&emsp;&emsp;status|评论审核状态(-1:审核失败,0:审核中,1:审核通过)|integer(int32)||
//|&emsp;&emsp;updateBy|更新人|string||
//|&emsp;&emsp;updateTime|更新日期|string(date-time)||
//|&emsp;&emsp;userId|用户主键ID|string||
//|success|成功标志|boolean||
//|timestamp|时间戳|integer(int64)|integer(int64)|
//
//
//**响应示例**:
//```javascript
//{
//    "code": 0,
//    "message": "",
//    "onlTable": "",
//    "result": {
//        "attachment": [
//            {
//                "cover": "",
//                "type": 0,
//                "url": ""
//            }
//        ],
//        "attachmentText": "",
//        "avatar": "",
//        "businessId": "",
//        "comment": "",
//        "contentId": "",
//        "createTime": "",
//        "id": "",
//        "isLikes": true,
//        "likes": 0,
//        "nickname": "",
//        "replyCommentId": "",
//        "replyNickname": "",
//        "replyText": "",
//        "replyUserId": "",
//        "review": "",
//        "status": 0,
//        "updateBy": "",
//        "updateTime": "",
//        "userId": ""
//    },
//    "success": true,
//    "timestamp": 0
//}
//```
//
